const express = require("express");
const cors = require("cors");

const rocketsRoutes = require("./server/rockets/routes");
const launchesRoutes = require("./server/launches/routes");

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

const port = 4000;

app.use("/launches", launchesRoutes);
app.use("/rockets", rocketsRoutes);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
