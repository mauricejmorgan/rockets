import * as React from 'react';
import { PropTypes } from 'prop-types';
import styled from 'styled-components';

const Row = styled.div`
  display: flex;
  flex-direction: col;
  flex-wrap: wrap;
  align-items: stretch;
`
const Col = styled.div`
  width: ${props => props.size};
  padding: 1rem;
  box-sizing: border-box;
`
const Grid = (props) => {
  return (
    <>
    { props.container && <Row>{props.children}</Row> }
    { props.item && <Col size={props.size}>{props.children}</Col> }
    </>
  )
}

export default Grid

Grid.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired,
  container: PropTypes.bool,
  items: PropTypes.bool,
  size: PropTypes.string,
}