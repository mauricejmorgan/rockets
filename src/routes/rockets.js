import { useState, useEffect } from "react";
import Wrapper from "../layout/wrapper";
import RockectCard from "../components/rocket-card";
import Grid from "../layout/grid";
import axios from "axios";

function RocketsRoute() {

  const [data, setData] = useState({ rockets: [] });
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {

      const rockects = await axios({
        url: "http://localhost:4000/rockets/",
        method: 'get'
      });
      setData({ rockets: rockects.data })
      setLoading(false);
    };
    fetchData();
  }, []);
  
  return (
    <>
    {loading && <div>loading....</div>} 
    {!loading && (<Wrapper>
    <Grid container>
      {data.rockets.map((item, index) => (
          <Grid item key={index.toString()} size='33.3%'> 
            <RockectCard
            image={item.flickr_images[0]}
            title={item.name}
            description={item.description}
            cost={item.cost_per_launch}
          />
         </Grid>
      ))}
    </Grid>
  </Wrapper>)}
  </>
  );
}

export default RocketsRoute;