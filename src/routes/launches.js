import { useState, useEffect } from "react";
import axios from "axios";
import styled from "styled-components";
import Wrapper from "../layout/wrapper";
import LaunchCard from "../components/launch-card";
import Grid from "../layout/grid";

const SearchBox = styled.div`
  display: flex;
  margin-left: 36px;

  button {
    border: none;
    padding: 10px;
    min-width: 100px;
    margin-left: 20px;
    cursor: pointer;
  }
`;

const Pagination = styled.div`
  width: 80%;
  margin: 0 auto;
  display: flex;
  justify-content: center;

  button {
    border: none;
    padding: 10px;
    min-width: 100px;
    margin-right: 10px;
    cursor: pointer;
  }
`;
const DISPLAY_ITEM_SIZE = 9;
const API_LIMIT = 50;

function LaunchesRoute() {

  const [data, setData] = useState({ launches: [] });
  const [loading, setLoading] = useState(true);
  //remove comment to limit call to 50
  const [api_limit] = useState(/*API_LIMIT*/);
  const [search, setSearch] = useState('');
  const [searchName, setSearchName] = useState();
  const [displayItemSize, setDisplayItemSize] = useState(DISPLAY_ITEM_SIZE);

  useEffect(() => {
    (async () => {
      const launches = (await axios({
        url: "http://localhost:4000/launches/",
        method: 'post',
        data: {
          searchFields: (searchName) ? searchName : undefined,
          options: (api_limit) ? { "offset": 0, "limit": api_limit } : { "pagination": false }
        }
      })).data;
      setData({ launches })
      setLoading(false);
    })();
  }, [api_limit, searchName]);

  const showMore = () => {
    const nextSize = displayItemSize + DISPLAY_ITEM_SIZE;
    const newSize = (nextSize <= data.launches.length) ? nextSize: data.launches.length;
    setDisplayItemSize(newSize);
  }

  return (
    <>
    {loading && <div>loading....</div>} 
    {!loading && (<Wrapper>
      <SearchBox>
        <input value={search} onChange={(e) => setSearch(e.target.value)}/>
        <button onClick={() => { setSearchName({ name: search })}}>Search</button>
    </SearchBox>
    <Grid container>

      { data.launches && 
        data.launches.slice(0, api_limit || displayItemSize).map((item, index) => (
          <Grid item key={index.toString()} size='33.3%'> 
            <LaunchCard
            image={item.links.patch.small}
            title={item.name}
            description={item.details}
          />
         </Grid>
      ))}
    </Grid>
    {!api_limit && 
        (<Pagination>
          <button onClick={showMore}>More</button>
        </Pagination>)
    }
  </Wrapper>)}
  </>
  );
}

export default LaunchesRoute;