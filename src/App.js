import "./App.css";
import { BrowserRouter as Router, Route, Switch  } from 'react-router-dom';
import styled from "styled-components";
import Header from "./components/shared/header";
import Hero from "./components/hero";
import Section from "./layout/section";
import LaunchesRoute from "./routes/launches";
import RocketsRoute from "./routes/rockets";


const MainWrapper = styled.main`
  display: block;
  position: relative;
  width: 100%;
`;

const ContentSelector = styled.div`
  width: 80%;
  margin: 0 auto;
  display: flex;
  justify-content: center;

  button {
    border: none;
    padding: 10px;
    min-width: 100px;
    margin-right: 10px;
  }
`;

function App() {
  return (
    <MainWrapper>
      <Header />
      <Section>
        <Hero />
      </Section>
      <Section>
        <ContentSelector>
          <button>Launches</button>
          <button>rockets</button>
        </ContentSelector>
      </Section>
      <Section>
        <Router>
          <Switch>
            <Route path="/" exact component={LaunchesRoute} />
            <Route path="/launches" exact component={LaunchesRoute} />
            <Route path="/rockets" exact component={RocketsRoute} />
          </Switch>
        </Router> 
      </Section>
    </MainWrapper>
  );
}

export default App;
