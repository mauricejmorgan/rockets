import { render, screen } from '@testing-library/react';
import LaunchCard from './index';

  test('renders and display correctly', () => {
    render(<LaunchCard 
      title="FalconSat" 
      description="Engine failure at 33 seconds and loss of vehicle"
      image="/images2.imgbox.com"/>);

    expect(screen.getByText(/Engine failure at 33 seconds and loss of vehicle/i)).toBeTruthy();
    expect(screen.getByText(/FalconSat/i)).toBeTruthy();
    expect(screen.getByRole('img')).toHaveAttribute('src', '/images2.imgbox.com');
  });
