import React from "react";
import styled from "styled-components";
import Image from "../image";
import Lockup from "../lockup";
import { PropTypes } from 'prop-types';

const RocketCardWrapper = styled.div`
  display: block;
  margin-bottom: 30px;
  margin-left: 20px;
  width: 100%;
  height: 100%;
`;

const RockCardContainer = styled.div`
  display: flex;
  flex-direction: column-reverse;
  height: 100%;
`;

const ImagContainer = styled.div`
  padding: 40px 20px;
  background-color: #b3c7cc;
  position: relative;
  margin-top: auto;

  img {
    height: 100px;
    width: auto;
    display: block;
    margin: 0 auto;
  }
`;

const Content = styled.div`
  padding: 20px;
  background-color: #f6f7f7;
  flex: 1;
  display: flex;
  flex-direction: column;
  flex-flow: wrap;
  align-content: space-between;
`;

const Cost = styled.div`
  margin-top: 15px;
  span {
    font-weight: 700;
  }
`;

function RocketCard(props) {
  return (
    <RocketCardWrapper>
      <RockCardContainer>
        <ImagContainer>
          <Image url={props.image} />
        </ImagContainer>

        <Content>
          <Lockup text={props.description} tag="h3" title={props.title} />
          <Cost><span>Launch Cost: </span> {props.cost}</Cost>
        </Content>
      
      </RockCardContainer>
    </RocketCardWrapper>
  );
}

export default RocketCard;

RocketCard.propTypes = {
  image: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  cost: PropTypes.string
}
