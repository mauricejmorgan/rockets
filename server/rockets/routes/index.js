const express = require('express');
const getRocketsController = require('../controllers/getRocketsController');
const router = express.Router();

router.get('/', getRocketsController);

module.exports = router;