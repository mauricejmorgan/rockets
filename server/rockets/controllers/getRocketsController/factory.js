module.exports = dependencies => async (req, res) => {
  const { getRocketsService } = dependencies;
  try {
    res.status(200).send(await getRocketsService());
  } catch (ex){
    res.status(500).send(ex.message);
  }
};