const factory = require('./factory');

const getRocketsService = require('../../services/getRocketsService');

const getRocketsController = factory({ getRocketsService });

module.exports = getRocketsController