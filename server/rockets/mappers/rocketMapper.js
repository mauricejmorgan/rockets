module.exports = (rocket) => ({
  name: rocket.name,
  description: rocket.description,
  cost_per_launch: Number(rocket.cost_per_launch).toLocaleString(
    'en-GB', { currency: "GBP",  style: "currency", minimumFractionDigits: 2 }
  ),
  flickr_images: rocket.flickr_images
})