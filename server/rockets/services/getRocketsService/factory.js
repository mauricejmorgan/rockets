const axios = require('axios');

const url = "https://api.spacexdata.com/v4/rockets";

module.exports = dependencies => async () => {
  const { rocketMapper } = dependencies;
  const allRockets = (await axios({ url, method: 'get' })).data;
  return allRockets.map(rocketMapper);
};