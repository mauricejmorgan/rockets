const factory = require('./factory');
const rocketMapper = require('../../mappers/rocketMapper');

const getRocketsServicesService = factory({ 
  rocketMapper 
});

module.exports = getRocketsServicesService