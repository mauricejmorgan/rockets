const factory = require('./factory');
const pastLaunchMapper = require('../../mappers/pastLaunchMapper');

const getPastLaunchesService = factory({ 
  pastLaunchMapper 
});

module.exports = getPastLaunchesService