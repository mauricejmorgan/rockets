const axios = require('axios');

const url = "https://api.spacexdata.com/v4/launches/query";

module.exports = dependencies => async (queryOptions) => {
  const { pastLaunchMapper } = dependencies;
  const response = (await axios({ url, method: 'post', data: queryOptions })).data; 
  return response.docs.map(pastLaunchMapper);
};