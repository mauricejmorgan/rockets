module.exports = (pastLaunch) => ({
  name: pastLaunch.name,
  details: pastLaunch.details,
  links: {
    patch: pastLaunch.links.patch,
    webcast: pastLaunch.links.webcast
  }
})