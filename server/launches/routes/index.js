const express = require('express');
const getPastLaunchesController = require('../controllers/getPastLaunchesController');
const router = express.Router();

router.post('/', getPastLaunchesController);

module.exports = router;