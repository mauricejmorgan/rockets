module.exports = dependencies => async (req, res) => {
  const { getPastLaunchesService } = dependencies;
  try {
    const { searchFields = {}, options } = req.body;
    const query = Object.keys(searchFields).map(field => ({
      [field]: { "$regex": searchFields[field], "$options": "i" }
    })).reduce((p, n) => ({...p, ...n}), {});
    res.status(200).send(await getPastLaunchesService({ query, options }));
  } catch (ex){
    res.status(500).send(ex.message);
  }
};