const factory = require('./factory');

const getPastLaunchesService = require('../../services/getPastLaunchesSevice');

const getPastLaunchesController = factory({ getPastLaunchesService });

module.exports = getPastLaunchesController