## _Full Stack Developer Task_

## UI Task
- Simple Grid Component. **DONE** 
- Card layout flip. **DONE**
- Added Prop-Type to components. **DONE**

## UI Task Bonus
- Align the grid with the top navigation. **NOT DONE**, (I wasn`t sure what you wanted here, and it wasn't clear from the art work).  
- Wrote basic test for Launch Card component. **DONE**

## Node Task
- `/launches` will return an array of 50 past launches including name, details, mission patch, and Youtube link. **DONE** 
- `/rockets` will return and array all the rockets including name, description, cost per launch (formatted), picture of the rocket. **DONE**

## Node Bonus Task
- `/launches` Show all the launches instead of 50 but with pagination. **DONE** (If you set api_limit to undefined the it will retrieve all the launches and pagination on the front-end)
- Create a search input with a submit button so the user will be able to search. () **DONE**
- Create 2 different routes accessible from the main navigation with react router that will show either the launches list or the rockets list. **DONE**